const express = require('express') // Import express
const router = express.Router() // Make router from app
const ReviewController = require('../controllers/reviewController.js') // Import TransaksiController
const reviewValidator = require('../middlewares/validators/reviewValidator.js') // Import validator to validate every request from user

router.get('/', ReviewController.getAll) // If accessing localhost:3000/transaksi, it will call getAll function in TransaksiController class
router.get('/:id_user', ReviewController.getOneByUser) // If accessing localhost:3000/transaksi/:id, it will call getOne function in TransaksiController class
router.get('/:id_movie', ReviewController.getOneByMovie)
router.post('/create', reviewValidator.create, ReviewController.create) // If accessing localhost:3000/transaksi/create, it will call create function in TransaksiController class
router.put('/update/:id', reviewValidator.update, ReviewController.update) // If accessing localhost:3000/transaksi/update/:id, it will call update function in TransaksiController class
router.delete('/delete/:id', ReviewController.delete) // If accessing localhost:3000/transaksi/delete/:id, it will call delete function in TransaksiController class

module.exports = router; // Export router
