const {
    review, movie, user
  } = require('../models');


  class ReviewController {

    async getAll(req, res) {
        review.find({}).then(result => {
          res.json({
            status: "success",
            data: result
          })
        });
      }

      async getOneByMovie(req, res) {
        movie.findOne({
          movie: req.params.movie.title
        }).then(result => {
          res.json({
            status: "success",
            data: result
          })
        })
      }

      async getOneByUser(req, res) {
        user.findOne({
          user: req.params.user.username
        }).then(result => {
          res.json({
            status: "success",
            data: result
          })
        })
      }

      async create(req, res) {
        const data = await Promise.all ([
          movie.findOne({
            _id: req.body.id_movie
          }),
          user.findOne({
            _id: req.body.id_user
          })
        ])


        review.create({
        "username": data[1].username
        "review": req.body.review,
        "rating": req.body.rating

        }).then(result => {
          res.json({
            status: "success",
            result: result
          })
        })
      }

      async update(req, res) {

        review.findOneAndUpdate({
          _id: req.params.id
        }, {
          "review": req.body.review,
          "rating": req.body.rating
        }).then(() => {
          return review.findOne({
            _id: req.params.id
          })
        }).then(result => {
          res.json({
            status: "success",
            result: result
          })
        })
      }

      async delete(req, res) {
        review.delete({
          _id: req.params.id
        }).then(() => {
          res.json({
            status: "success",
            data: null
          })
        })
      }

  }

  module.exports = new ReviewController;
