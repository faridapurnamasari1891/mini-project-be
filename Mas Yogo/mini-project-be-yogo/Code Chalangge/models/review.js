const mongoose = require("mongoose");
const mongoose_delete = require('mongoose-delete');

const ReviewSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.Mixed,
    required: true
  },
  movie: {
    type: mongoose.Schema.Types.Mixed,
    required: true
  },
  review: {
    type: String,
    required: true
  },
  rating: {
    type: Number,
    required: true
  },
}, {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  },
  versionKey: false
});

ReviewSchema.plugin(mongoose_delete, {
  overrideMethods: 'all'
});

module.exports = review = mongoose.model("review", ReviewSchema, "review");
