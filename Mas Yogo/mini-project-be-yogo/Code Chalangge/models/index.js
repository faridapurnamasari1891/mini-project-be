const mongoose = require('mongoose');

const uri = "mongodb://localhost:27017/Review"

mongoose.connect(uri, { useUnifiedTopology: true, useNewUrlParser: true })

const review = require('./review.js')

module.exports = { review };
