const { review } = require('../../models')
const {
  check,
  validationResult,
  matchedData,
  sanitize
} = require('express-validator'); //form validation & sanitize form params

module.exports = {
  create: [
    //Set form validation rule
    check('id_movie').custom(value => {
      return movie.findById(value).then(b => {
        if (!b) {
          throw new Error('movie does not exist');
        }
      })
    }),
    check('id_user').custom(value => {
      return user.findById(value).then(p => {
        if (!p) {
          throw new Error('user does not exist!');
        }
      })
    }),
    check('review').isString(),
    check('rating').custom(value => {
      return value.then(c =>{
        if (c>=10 && c<0){
          throw new Error ('rating is not valid')
        }
      })
      })
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
  ],

  update: [
    //Set form validation rule
    check('id_movie').custom(value => {
      return movie.findById(value).then(b => {
        if (!b) {
          throw new Error('movie does not exist');
        }
      })
    }),
    check('id_user').custom(value => {
      return user.findById(value).then(p => {
        if (!p) {
          throw new Error('user does not exist!');
        }
      })
    }),
    check('review').isString(),
    check('rating').custom(value => {
      return value.then(c =>{
        if (c>=10 && c<0){
          throw new Error ('rating is not valid')
        }
      })
      })
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
  ],

  delete: [
    check('id').custom(value => {
      return review.findOne({
        _id: value
      }).then(result => {
        if (!result) {
          throw new Error('ID review tidak ada!')
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
  ]
};
