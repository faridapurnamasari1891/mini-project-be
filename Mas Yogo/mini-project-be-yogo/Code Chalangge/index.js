// Express
const express = require('express');
const app = express();
const bodyParser = require('body-parser'); //post body handler
const reviewRoutes = require('./routes/reviewRoutes.js')

//Set body parser for HTTP post operation
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({
  extended: true
})); // support encoded bodies

//set static assets to public directory
app.use(express.static('public'));

app.use('/review', reviewRoutes) // if accessing localhost:3000/review/* we will go to reviewRoutes

app.listen(3000, () => console.log("server running on http://localhost:3000"))
