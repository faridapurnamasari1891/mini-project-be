const mongoose = require('mongoose') // Import mongoose

const uri = "mongodb://localhost:27017/UserData" // Database url in mongodb

mongoose.connect(uri, { useUnifiedTopology: true, useNewUrlParser: true }) // Make connection to mongodb penjualan_dev database

const user = require('./user') // Import user model


module.exports = { user }; // Exports all models
