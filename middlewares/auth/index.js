const passport = require('passport')
const localStrategy = require('passport-local').Strategy; // Import Strategy
const {
  user
} = require('../../models') // Import user model
const bcrypt = require('bcrypt'); // Import bcrypt
const JWTstrategy = require('passport-jwt').Strategy; // Import JWT Strategy
const ExtractJWT = require('passport-jwt').ExtractJwt; // Import ExtractJWT

// If user sign up
passport.use(
  'signup',
  new localStrategy({
      'usernameField': 'email', // field for username from req.body.email
      'passwordField': 'password', // field for password from req.body.password
      passReqToCallback: true // read other requests
    },
    async (req, email, password, done) => {
      // Create user data
      user.create({
        username: req.body.username,
        email: req.body.email, // email get from usernameField (req.body.email)
        password: bcrypt.hashSync(req.body.password, 10), // password get from passwordField (req.body.passport)
        name: req.body.name,
        profilePic:"default-avatar.jpg"
    //    role: "visitor" // role get from req.body.role
      }).then(result => {
        // If success, it will return authorization with req.user
        return done(null, result, {

          message: 'User created!'
        })
      }).catch(err => {
        // If error, it will return not authorization
        return done(null, false, {
          message: "anda gagal!"
        })
      })
    },
  )
);

// If user signin
passport.use(
  'signin',
  new localStrategy({
      'usernameField': 'email', // username from req.body.email
      'passwordField': 'password' // password from req.body.password
    },
    async (email, password, done) => {
      // find user depends on email
      const userLogin = await user.findOne({

        email: email

    }
    )


      // if user not found
      if (!userLogin) {
        return done(null, false, {
          message: 'User is not found!'
        })
      }

      // if user found and validate the password of user
      const validate = await bcrypt.compare(password, userLogin.password);

      // if wrong password
      if (!validate) {
        return done(null, false, {
          message: 'Wrong password!'
        })
      }

      // signin success
      return done(null, userLogin, {
        message: 'Login success!'
      })
    }
  )
)

// Strategy for transaction role
passport.use(
  'visitor',
  new JWTstrategy({
      secretOrKey: 'secret_password', // key for jwt
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken() // extract token from authorization
    },
    async (token, done) => {
      // find user depend on token.user.email
      const userLogin = await user.findOne({

        email: token.user.email

      })

      // if user.role includes transaksi it will next
      if (userLogin.role.includes('visitor')) {
        return done(null, token.user)
      }

      // if user.role not includes transaksi it will not authorization
      return done(null, false)
    }
  )
)

passport.use(
  'admin',
  new JWTstrategy({
      secretOrKey: 'secret_password', // key for jwt
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken() // extract token from authorization
    },
    async (token, done) => {
      // find user depend on token.user.email
      const userLogin = await user.findOne({

        email: token.user.email

      })

      // if user.role includes transaksi it will next
      if (userLogin.role.includes('admin')) {
        return done(null, token.user)
      }

      // if user.role not includes transaksi it will not authorization
      return done(null, false)
    }
  )
)
