const express = require('express'); // import express
const router = express.Router(); // import router
const passport = require('passport'); // import passport
const auth = require('../middlewares/auth/index'); // import passport auth strategy
const UserController = require('../controllers/userController'); // import userController
const userValidator = require('../middlewares/validators/userValidator'); // import userValidator

// if user go to localhost:3000/signup

router.post('/signup', [userValidator.signup, passport.authenticate('signup', {
  session: false
})],  UserController.signup);
// if user go to localhost:3000/signin
router.post('/signin',[userValidator.signin, passport.authenticate('signin', {
  session: false
})], UserController.signin);
router.put('/edit/:id',userValidator.update, UserController.update)
router.delete('/delete/:id',userValidator.delete , UserController.delete)

//router.put('/edit', userValidator.update, UserController.update)

module.exports = router; // export router
