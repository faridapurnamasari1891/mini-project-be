const { user } = require('../models/') // import user models
const passport = require('passport'); // import passport
const jwt = require('jsonwebtoken'); // import jsonwebtoken
const { ObjectId } = require('mongodb') // Import ObjectId from mongodb

class UserController {
  async getAll(req, res) {
    // Find all transaksi collection data
    user.find({}).then(result => {
      res.json({
        status: 'success',
        data: result
      })
    })
  }

  // if user signup
  async signup(req, res) {
    // get the req.user from passport authentication
    const body = {
      email: req.body.email,
    };

    // create jwt token from body variable
    const token = jwt.sign({
      user: body
    }, 'secret_password',{expiresIn:'1d'})

    // success to create token
    res.status(200).json({
      message: 'Sign up success!',
      token: token
    })
  }

  // if user signin
  async signin(req, res) {
    // get the req.user from passport authentication
    const body = {
        email: req.user.email
    };

    // create jwt token from body variable
    const token = jwt.sign({
      user: body
    }, 'secret_password',{expiresIn:'1d'})

    // success to create token
    res.status(200).json({
      message: 'Sign in success!',
      token: token
    })
  }



    async update(req,res) {

      user.findOneAndUpdate({
        _id:req.params.id
      }, {
        username:req.body.username,
        name:req.body.name,
        email:req.body.email,
        password:req.body.password,
        profilePic: req.file===undefined?"":req.file.filename
      }).then(()=>{
        return user.findOne({
          _id:req.params.id
        })
      }).then(result=>{
        res.json({
          status:"Success updating data",
          data:result
        })
      })
    }

    async delete(req, res) {
      user.delete({
        _id: req.params.id
      }).then(() => {
        res.json({
          status: "success",
          data: null
        })
      })
    }


  }



module.exports = new UserController; // export UserController
